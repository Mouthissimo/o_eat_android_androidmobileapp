package com.bgiaa.o_eat_arduino_androidmobileapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Glaciere implements Parcelable {
    private String serial_no;
    private int temperature;
    private double coordX;
    private double coordY;

    public Glaciere(String conSerial_no, int conTemperature, double conCoordX, double conCoordY){
        serial_no = conSerial_no;
        temperature = conTemperature;
        coordX = conCoordX;
        coordY = conCoordY;
    }

    protected Glaciere(Parcel in) {
        serial_no = in.readString();
        temperature = in.readInt();
        coordX = in.readDouble();
        coordY = in.readDouble();
    }

    public static final Creator<Glaciere> CREATOR = new Creator<Glaciere>() {
        @Override
        public Glaciere createFromParcel(Parcel in) {
            return new Glaciere(in);
        }

        @Override
        public Glaciere[] newArray(int size) {
            return new Glaciere[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serial_no);
        dest.writeInt(temperature);
        dest.writeDouble(coordX);
        dest.writeDouble(coordY);
    }

    public String getSerial_no(){
        return serial_no;
    }

    public int getTemperature(){
        return temperature;
    }

    public double getCoordX(){
        return coordX;
    }

    public double getCoordY(){
        return coordY;
    }
}
