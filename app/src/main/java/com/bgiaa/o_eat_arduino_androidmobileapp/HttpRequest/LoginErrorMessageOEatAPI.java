package com.bgiaa.o_eat_arduino_androidmobileapp.HttpRequest;

public class LoginErrorMessageOEatAPI {

    public MessageClass message;
    public String status;

    public class MessageClass
    {
        public int Code;
        public String Message;
        public FieldMessagesClass FieldMessages;

        public class FieldMessagesClass
        {
            public String Password;
        }
    }
}
