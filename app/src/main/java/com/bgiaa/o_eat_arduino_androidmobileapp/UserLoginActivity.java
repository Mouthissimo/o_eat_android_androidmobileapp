package com.bgiaa.o_eat_arduino_androidmobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bgiaa.o_eat_arduino_androidmobileapp.HttpRequest.LoginErrorMessageOEatAPI;
import com.bgiaa.o_eat_arduino_androidmobileapp.HttpRequest.UnsafeOkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;

public class UserLoginActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor sharedPreferencesEditor;

    String apiUrl;
    EditText loginEmail;
    EditText loginPassword;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        sharedPreferences = getApplicationContext().getSharedPreferences("OEatAppSharedPreferences", MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();

        //  Remove headbar
        try
        {
            this.getSupportActionBar().hide();
        }
        catch(NullPointerException e){

        }

        //  Variables Setup
        apiUrl = getString(R.string.api_url);
        loginEmail = findViewById(R.id.editTextEmail);
        loginPassword = findViewById(R.id.editTextPassword);
        loginButton = findViewById(R.id.buttonLogin);

        //  FAN Setup
        OkHttpClient unsafeOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        AndroidNetworking.initialize(getApplicationContext(), unsafeOkHttpClient);
        //AndroidNetworking.initialize(getApplicationContext());


        //  Login Button setup
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputEmailValue = loginEmail.getText().toString();
                String inputPasswordValue = loginPassword.getText().toString();

                inputEmailValue = "adrien.charneau@edu.itescia.fr";
                inputPasswordValue = "sedDJ*xtyN47";

                //inputEmailValue = "wrongEmail";
                //inputPasswordValue = "wrongPassword";

                loginPostFAN(inputEmailValue, inputPasswordValue);
            }
        });
    }

    // Login Action with FAN
    private void loginPostFAN(String emailInput, String passwordInput) {
        String subdomain = getString(R.string.tenant_subdomain);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Username", emailInput);
            jsonObject.put("Password", passwordInput);
            jsonObject.put("TenantSubdomain", subdomain);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(apiUrl + "api/user/login")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("Origin", "https://oeat.oneproteus.com")
                .addJSONObjectBody(jsonObject) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //toastErrorMessage("it works");

                        try {
                            JSONObject data = response.getJSONObject("data");

                            sharedPreferencesEditor.putString("refresh", data.getString("refresh"));
                            sharedPreferencesEditor.putString("token", data.getString("token"));
                            sharedPreferencesEditor.apply();

                            Intent intent = new Intent(UserLoginActivity.this, GlaciereListActivity.class);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        LoginErrorMessageOEatAPI apiError;
                        LoginErrorMessageOEatAPI.MessageClass message;
                        String status;
                        String Message;

                        if (error.getErrorCode() != 0) {
                            if (error.getErrorCode() == 400){
                                apiError = error.getErrorAsObject(LoginErrorMessageOEatAPI.class);
                                message = apiError.message;
                                status = apiError.status;
                                Message = message.Message;
                                //LoginErrorMessageOEatAPI.MessageClass.FieldMessagesClass FieldMessages = message.FieldMessages;
                                //String password = FieldMessages.Password;
                                toastErrorMessage(status + ": " + Message);
                            }
                            else{
                                apiError = error.getErrorAsObject(LoginErrorMessageOEatAPI.class);
                                message = apiError.message;
                                status = apiError.status;
                                Message = message.Message;
                                toastErrorMessage(status + ": " + Message);
                                //toastErrorMessage("errorCode : " + error.getErrorCode() + "\n\n" + "errorBody : " + error.getErrorBody() + "\n\n" + "errorDetail : " + error.getErrorDetail());
                            }

                        } else {
                            toastErrorMessage(error.getErrorDetail()); // : connectionError, parseError, requestCancelledError
                        }
                    }
                });
    }

    //  Error message display
    private void toastErrorMessage(String message){
        Toast toastTest = Toast.makeText(UserLoginActivity.this, message, Toast.LENGTH_LONG);
        toastTest.setGravity(Gravity.BOTTOM, 0, 200);
        toastTest.show();
    }
}
