package com.bgiaa.o_eat_arduino_androidmobileapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bgiaa.o_eat_arduino_androidmobileapp.GlaciereInfoActivity;
import com.bgiaa.o_eat_arduino_androidmobileapp.Models.Glaciere;
import com.bgiaa.o_eat_arduino_androidmobileapp.R;

import java.util.ArrayList;

public class GlaciereAdapter extends RecyclerView.Adapter<GlaciereAdapter.MyViewHolder> {

    Context context;
    private ArrayList<Glaciere> glacieresList;

    public GlaciereAdapter(Context conContext, ArrayList<Glaciere> conGlacieresList){
        context = conContext;
        glacieresList = conGlacieresList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.glaciere_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.glaciere = glacieresList.get(position);
        holder.serial_noTextView.setText(glacieresList.get(position).getSerial_no());
    }

    @Override
    public int getItemCount() {
        return glacieresList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        Glaciere glaciere;
        TextView serial_noTextView;
        Button infoButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            serial_noTextView = itemView.findViewById(R.id.serial_no_TextView);
            infoButton = itemView.findViewById(R.id.infoButton);
            infoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, GlaciereInfoActivity.class);
                    intent.putExtra("glaciere", glaciere);
                    context.startActivity(intent);
                }
            });

        }
    }
}
